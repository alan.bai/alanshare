package base

import "sync"

var set map[string]int

type SafeMap struct {
	data map[string]int
	sync.Mutex

	sMap sync.Map
}
