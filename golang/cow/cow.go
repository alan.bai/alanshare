package cow

import (
	"sync"
	"sync/atomic"
)

// Simple c-o-w used in localCache
type CopyOnWrite interface {
	Read(k string) (interface{}, bool)
	Write(k string, v interface{})
	Load(m Map)
}

type Map map[string]interface{}

type copyOnWrite struct {
	sync.Mutex
	m atomic.Value
}

func NewCopyOnWrite() CopyOnWrite {
	var m atomic.Value
	m.Store(make(Map))
	return &copyOnWrite{
		m: m,
	}
}

func (c *copyOnWrite) Read(k string) (interface{}, bool) {
	m := c.m.Load().(Map)
	v, ok := m[k]
	return v, ok
}

func (c *copyOnWrite) Write(k string, v interface{}) {
	c.Lock()
	defer c.Unlock()
	m := c.m.Load().(Map)
	newMap := make(Map, len(m))

	for k, v := range m {
		newMap[k] = v
	}

	newMap[k] = v
	c.m.Store(newMap)
}

func (c *copyOnWrite) Load(m Map) {
	c.m.Store(m)
}
