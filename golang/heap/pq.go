package heap

import "container/heap"

type Item interface {
	Less(item Item) bool
}

type PQ []Item

func (h PQ) Len() int           { return len(h) }
func (h PQ) Less(i, j int) bool { return h[i].Less(h[j]) }
func (h PQ) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *PQ) Push(x interface{}) {
	*h = append(*h, x.(Item))
}

func (h *PQ) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

type PriorityQueue struct {
	PQ
}

func NewPriorityQueue() *PriorityQueue {
	var pq PriorityQueue
	heap.Init(&pq.PQ)
	return &pq
}

func (pq *PriorityQueue) Push(item Item) {
	heap.Push(&pq.PQ, item)
}

func (pq *PriorityQueue) Pop() Item {
	return heap.Pop(&pq.PQ).(Item)
}

func (pq *PriorityQueue) Front() Item {
	return pq.PQ[0]
}

func (pq *PriorityQueue) Length() int {
	return pq.PQ.Len()
}
