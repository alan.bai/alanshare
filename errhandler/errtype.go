package errhandler

import (
	"errors"
	"fmt"
)

type errString string

func (e errString) Error() string {
	return string(e)
}

type errStruct struct {
	s string
}

func (e *errStruct) Error() string {
	return e.s
}

func A() error {
	var err error
	err = errString("s")
	err = &errStruct{}

	err = errors.New("error")
	err = fmt.Errorf("err [%w]", err)

	return err
}

func handleA() error {
	err := A()
	switch err.(type) {
	case nil:
	case *errStruct:
	case errString:
	default:
	}

	se := new(errStruct)
	errors.As(err, &se)
	fmt.Println(se.s)
	return nil
}
