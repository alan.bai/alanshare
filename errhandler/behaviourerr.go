package errhandler

import (
	"net"
	"time"
)

type notFound interface {
	NotFound() bool
}

func Behavior() error {
	err := A()
	notfound, ok := err.(notFound)
	if ok && notfound.NotFound() {
		return nil
	}

	notfound, ok = err.(interface {
		NotFound() bool
	})

	if ok && notfound.NotFound() {
		return nil
	}

	return err
}

func NetErr() {
	var err error
	if nerr, ok := err.(net.Error); ok && nerr.Temporary() {
		time.Sleep(time.Second)
	}
}
