package main

import (
	"Cabital/myworkspace/alanshare/errhandler"
	"github.com/pkg/errors"
	"github.com/tal-tech/go-zero/core/logx"
)

func main() {
	err := errhandler.Second()
	logx.Errorf("%T %v\n", errors.Cause(err), errors.Cause(err))
	logx.Errorf("\n%+v\n", err)
}
