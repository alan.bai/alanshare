package errhandler

import "github.com/pkg/errors"

var ErrSentinel = errors.New("sentinel error")
