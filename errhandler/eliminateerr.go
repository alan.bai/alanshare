package errhandler

import "strconv"

func Bad() error {
	err := A()
	if err != nil {
		return err
	}
	return nil
}

func Good() error {
	return A()
}

func Bad1() error {
	for i := 0; i < 10; i++ {
		_, err := strconv.Atoi("aaa")
		if err != nil {
			return err
		}
	}
	return nil
}

func convert(s string) error {
	_, err := strconv.Atoi(s)
	return err
}

func Good1() error {
	for i := 0; i < 10; i++ {
		if err := convert("a"); err != nil {
			return err
		}
	}
	return nil
}
