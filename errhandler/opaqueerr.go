package errhandler

import (
	"github.com/pkg/errors"
	"github.com/tal-tech/go-zero/core/stores/sqlx"
)

func Err() (int, error) {
	return 0, sqlx.ErrNotFound
}

func opaque() error {
	_, err := Err()
	if err != nil {
		return err
	}
	return nil
}

func handle() error {
	_, err := Err()
	switch err {
	case sqlx.ErrNotFound:
	}

	if errors.Is(err, sqlx.ErrNotFound) {
		return nil
	}
	return err
}
