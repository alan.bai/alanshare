package errhandler

import "github.com/pkg/errors"

func First() (int, error) {
	return 0, errors.Wrap(errors.New("first error"), "First")
}

func Second() error {
	_, err := First()
	if err != nil {
		return errors.WithMessage(err, "Second")
	}
	return err
}
