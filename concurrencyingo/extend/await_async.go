package extend

import (
	"Cabital/myworkspace/alanshare/concurrencyingo/extend/async"
	"fmt"
	"time"
)

func DoneAsync() chan int {

	r := make(chan int)

	fmt.Println("Warming up ...")

	go func() {

		time.Sleep(3 * time.Second)

		r <- 1

		fmt.Println("Done ...")

	}()
	return r
}

func DoneAsyncAwait() int {

	fmt.Println("Warming up ...")

	time.Sleep(3 * time.Second)

	fmt.Println("Done ...")

	return 1

}

func main() {

	fmt.Println("Let's start ...")

	val := DoneAsync()

	fmt.Println("Done is running ...")

	fmt.Println(<-val)
}

func main1() {

	fmt.Println("Let's start ...")

	future := async.Exec(func() interface{} {

		return DoneAsync()

	})

	fmt.Println("Done is running ...")

	val := future.Await()

	fmt.Println(val)

}
