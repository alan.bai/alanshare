[TOC]

## 进程and线程and协程

### 问题

#### 并发和并行有什么区别？

​		并发不是并行，并行指多个线程同时在不同处理器执行代码，而并发是在同一个处理器交替执行

#### 已经有线程了为什么还需要协程？

1. 线程是为了让操作系统并发运行程序，以达到”同时“运行更多程序的目的，而协程是为了让一个线程内的程序并发服务更多内容
2. 避免了陷入内核级别的上下文切换造成的性能损失，进而突破了线程在IO上的性能瓶颈

#### 既然协程这么好，为啥java没有呢？

1. 协程能做的事情，线程都可以做
2. 并不是每个线程切换都很耗资源

https://www.zhihu.com/question/332042250

### 相关概念

- 操作系统为应用程序创建进程，进程管理应用程序资源，包括内存地址空间，文件句柄，设备和线程
- 线程是操作系统调度的执行流程或者说是执行路径，在处理器中执行代码
- 一个进程从一个线程开始，即主线程，主线程可以创建更多的子线程
- 协程是在线程内部，由程序控制转换执行流程，交替运行代码片段，线程和协程的区别主要是数量上的，而不是性质上，所以说协程从逻辑上来说也是线程。

## 并发编程的困难

### 问题

#### i++是原子的吗？

#### redis哪些操作是原子性的？

### 数据竞争

### 死锁，活锁，锁的饥饿问题

死锁是所有并发进程都在彼此等待的状态。 在这种情况下，如果没有外部干预，程序将永远不会恢复。

活锁是正在主动执行并发操作的程序，但这些操作无法向前移动程序的状态。

饥饿是指并发进程无法获得执行工作所需的任何资源的情况。

## 并发(同步)原语

​		解决并发问题的一个基础的数据结构

### 相关概念

#### 临界区

​		临界区就是一个被共享的资源，或者说是一个整体的一组共享资源，比如对数据库的访问、对某一个共享数据结构的操作、对一个 I/O 设备的使用、对一个连接池中的连接的调用，等等。

#### 原子操作

原子操作(atomic operation)是指不会被线程调度机制打断的操作；这种操作一旦开始，就一直运行到结束，中间不会有任何 context switch （切换到另一个线程）。保证访问共享资源是唯一的。

#### CAS (compare-and-swap，或 compare-and-set)

FFA TAS

#### 原子性 

​		CPU提供基础原子操作，不同架构系统原子操作不一样

#### 互斥锁（排它锁）

#### 读写锁 

#### Barrier (go 中waitgroup)

#### wait/notify (go 中cond)

## Go中的并发

#### go中内存模型

由于 CPU 指令重排和多级 Cache 的存在，保证多核访问同一个变量这件事儿变得非常复杂。毕竟，不同 CPU 架构（x86/amd64、ARM、Power 等）的处理方式也不一样，再加上编译器的优化也可能对指令进行重排，所以编程语言需要一个规范，来明确多线程同时访问同一个变量的可见性和顺序

#### Happens-before

在一个 goroutine 内部，程序的执行顺序和它们的代码指定的顺序是一样的，即使编译器或者 CPU 重排了读写顺序，从行为上来看，也和代码指定的顺序一样。

#### goroutine

**Never start a goroutine without knowning when it will stop**

### csp

*Go* 的并发原语 *goroutines* 和 *channels* 为构造并发软件提供了一种优雅而独特的方法。*Go* 没有显式地使用锁来协调对共享数据的访问，而是鼓励使用 *chan* 在 *goroutine* 之间传递对数据的引用。这种方法确保在给定的时间只有一个*goroutine* 可以访问数据。

### mutex

- 大部分场景
- 共享资源的并发访问使用传统并发原语

### rwmutex

- 读多写少

### Waitgroup

- 任务编排

### cond 基本不用，可用channel代替

### once

- ​	单例初始化，懒加载，初始化外部资源，启动弱依赖，避免强依赖

### channel 

Don’t communicate by sharing memory, share memory by communicating.

执行业务处理的 goroutine 不要通过共享内存的方式通信，而是要通过 Channel 通信的方式分享数据。

无缓冲信道的本质是保证同步。

有缓冲好处*:* 延迟更小。

代价*:* 不保证数据到达，越大的 *buffer*，越小的保障到达。*buffer = 1* 时，给你延迟一个消息的保障。

- 任务编排

- 数据传递

- 信号同志

- 状态转移

- 锁

  ```go
  func ListDirectory(dir string) ([]string, error)
  func ListDirectory(dir string) chan string
  func ListDirectory(dir string fn func(string)) chan string
  ```

```go
func leak() {
  ch := make(chan int)
  go func() {
    val := <- ch
  }()
}
```

#### 坑点

- close 为 nil 的 chan
- send 已经 close 的 chan
- close 已经 close 的 chan

### context

- 超时控制
- 信息穿透
- 控制协程生命周期

### sync.Map

- 很少用
- 只会增长的缓存系统，只写入一次而被读很多次
- 多个goroutine读不同的key

### sync.pool

用来保存和复用临时对象，以减少内存分配，降低 *GC* 压力*(Request-Driven* 特别合适*)*。

- 池化

### atomic

- 配置文件的读写
- 状态的原子操作

### 扩展

- 无锁 lock-free

- COW copy-on-write

   https://juejin.cn/post/6844903702373859335

   https://zhuanlan.zhihu.com/p/339437815

- async-await

- semaphore

- singleflight 

- 归并回源

  防止缓存击穿

  CDN原理 https://www.jianshu.com/p/e7751ecb6f21

- cyclicbarrier 用来控制一组请求同时执行的数据结构

- errgroup

## 分布式中的并发

### 问题

#### etcd的集群节点数为啥是3个5个？不是4个8个10个？

拜占庭容错 3n+1（raft paxos）2n + 1

#### 分布式系统扩展的方式？复制

#### 抛开etcd，你会怎么实现微服务的服务注册功能？

kafka 

rocketmq namers

### leader选举

适用于主从架构，主节点写，从节点读

### 互斥锁

redis

### 读写锁

### 栅栏

一组节点协同工作，共同等待一个信号，在信号未出现前，这些节点会被阻塞住，而一旦信号出现，这些阻塞的节点就会同时开始继续执行下一步的任务。

Barrier 分布式栅栏

DoubleBarrier 计数型栅栏

## 案例分享

### pricing服务之upstream job

#### 场景：

定时从第三方获取数字货币源头价格，清洗并入库，单节点无法保证可用性，增加节点保证可用性

#### 技术点：分布式，主从模式，etcd，redis缓存设计

### pricing服务之quote job

#### 场景：

根据upstream价格流数据，生成我方相应报价，任务粒度按 **币种对-Purpose** 切分

#### 技术点：分布式，切分任务，etcd，分布式锁，一致性，异常处理，mysql存储设计

### hedging服务

#### 场景：

我方持有资金的风险对冲系统，需要将交易数据按某种策略（定时或者水位）汇总起来，然后创建order交易

#### 技术点： 分布式，redis，mq，cas，原子操作，localcache，桶，延时任务，异步操作计算存储分离

## Ref

https://www.cs.cmu.edu/~crary/819-f09/Hoare78.pdf CSP论文

https://github.com/smallnest/dive-to-gosync-workshop

https://github.com/kat-co/concurrency-in-go-src 《concurrency in go》

https://github.com/uber-go/atomic atomic扩展

https://www.cs.rochester.edu/u/scott/papers/1996_PODC_queues.pdf 无锁队列论文

https://www.kancloud.cn/mutouzhang/go/598655 《concurrency in go中文笔记》

http://marcio.io/2015/07/handling-1-million-requests-per-minute-with-golang/ 

https://golang.org/ref/mem go中内存模型

https://www.youtube.com/watch?v=f6kdp27TYZs

https://www.youtube.com/watch?v=QDDwwePbDtw

https://medium.com/@cep21/how-to-correctly-use-context-context-in-go-1-7-8f2c0fafdf39 context

https://golang.org/doc/effective_go.html#concurrency effective_go

https://medium.com/a-journey-with-go/go-how-to-reduce-lock-contention-with-the-atomic-package-ba3b2664b549

https://www.ardanlabs.com/blog/2017/10/the-behavior-of-channels.html

https://medium.com/a-journey-with-go/go-ordering-in-select-statements-fd0ff80fd8d6

https://www.ardanlabs.com/blog/2018/11/goroutine-leaks-the-forgotten-sender.html

https://segmentfault.com/a/1190000019973632 sync.pool

https://golang.design/under-the-hood/ golang源码分析

https://geektutu.com/post/hpg-sync-pool.html go语言高性能编程

https://blog.csdn.net/qq_20597727/article/details/88652045 redis缓存一致性