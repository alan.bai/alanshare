package main

import (
	"Cabital/myworkspace/alanshare/concurrencyingo/base/mutex"
	"fmt"
	"sync"
)

func badCount() {
	var count = 0
	var wg sync.WaitGroup
	wg.Add(10)
	for i := 0; i < 10; i++ {
		go func() {
			defer wg.Done()
			for j := 0; j < 100000; j++ {
				count++
			}
		}()
	}
	wg.Wait()
	fmt.Println(count)
}

func mutexCount() {

	var mu sync.Mutex
	var count = 0

	var wg sync.WaitGroup
	wg.Add(10)

	for i := 0; i < 10; i++ {
		go func() {
			defer wg.Done()
			for j := 0; j < 100000; j++ {
				mu.Lock()
				count++
				mu.Unlock()
			}
		}()
	}
	wg.Wait()
	fmt.Println(count)
}

func badCopyMutex() {
	var c mutex.Counter
	c.Lock()
	defer c.Unlock()
}

func copyCounter(c mutex.Counter) {
	c.Lock()
	defer c.Unlock()
}

func main() {
	badCount()
}
