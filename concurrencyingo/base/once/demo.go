package once

import "sync"

var threeOnce struct {
	sync.Once
	v int
}

func three() int {
	threeOnce.Do(func() {
		threeOnce.v = 3
	})
	return threeOnce.v
}
