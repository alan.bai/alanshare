package pool

import (
	"fmt"
	"sync"
)

func main() {
	p := &sync.Pool{
		New: func() interface{} {
			return "test"
		},
	}

	s := p.Get().(string)
	fmt.Println(s)
	p.Put(s)
}
