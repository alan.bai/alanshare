package mutex

import "sync"

type Counter struct {
	CounterType int
	Name        string
	sync.Mutex
	count uint64
}

func (c *Counter) Incr() {
	c.Lock()
	c.count++
	c.Unlock()
}

func (c *Counter) Count() uint64 {
	c.Lock()
	defer c.Unlock()
	return c.count
}
